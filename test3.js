/**
 * Using fs, Write into a file ./test.txt the sentence "Hello World!" in this project's root folder.
 * When the write is complete, console.log that the file was saved successfully.
 */


// Initialize fs
var fs = require('fs');

fs.writeFile("/test.txt", "Hello World!", function(err) {

    // error
    if(err) {
        return console.log(err);
    }

    // success
    console.log("The file was saved successfully");
}); 